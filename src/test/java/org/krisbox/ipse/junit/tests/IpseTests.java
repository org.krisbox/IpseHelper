package org.krisbox.ipse.junit.tests;

import org.junit.Test;
import org.krisbox.ipse.junit.ipse.DummyIpse;
import org.krisbox.ipse.junit.utils.MockRequest;

import static org.junit.Assert.assertEquals;

public class IpseTests {
    DummyIpse   dummyIpse;
    MockRequest mockRequest;

    String controlCookieName   = "ipseJSON";
    String controlCookieString = "{\"ipseProperties\": {\"username\": \"connorjay\",\"password\": \"pass\",\"volume\": \"Default Volume\",\"volumeProfile\": \"Default Volume\",\"extendedCredentials\": \"stuff\",\"enterprise\": true,\"userHomeFolder\": \"/home/connorjay\",\"sessionLevelParameters\": {\"parameterNames\": [\"testParam0\", \"testParam1\"],\"parameterValues\": [\"testValue0\", \"testValue1\"]}}}";


    public IpseTests() {
        dummyIpse   = new DummyIpse(controlCookieName);
        mockRequest = new MockRequest(controlCookieName, controlCookieString);

        // Authenticate once to set everything
        dummyIpse.authenticate(mockRequest.createMockRequest());
    }

    @Test
    public void testAuthenticate() {
        assertEquals(true, dummyIpse.authenticate(mockRequest.createMockRequest()));
    }
    @Test
    public void testUsername() {
        assertEquals(mockRequest.getUsername(),       dummyIpse.getUserName());
    }
    @Test
    public void testPassword() {
        assertEquals(mockRequest.getPassword(),       dummyIpse.getPassword());
    }
    @Test
    public void testVolume() {
        assertEquals(mockRequest.getVolume(),         dummyIpse.getVolume());
    }
    @Test
    public void testVolumeProfile() {
        assertEquals(mockRequest.getVolumeProfile(),  dummyIpse.getVolumeProfile());
    }
    @Test
    public void testEnterprise() {
        assertEquals(mockRequest.getEnterprise(),     dummyIpse.isEnterprise());
    }
    @Test
    public void testUserHomeFolder() {
        assertEquals(mockRequest.getUserHomeFolder(), dummyIpse.getUserHomeFolder());
    }
}