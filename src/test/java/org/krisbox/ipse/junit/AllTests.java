package org.krisbox.ipse.junit;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.krisbox.ipse.junit.tests.IpseTests;
import org.krisbox.ipse.junit.tests.PojoTests;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        PojoTests.class,
        IpseTests.class
})
public class AllTests {
}