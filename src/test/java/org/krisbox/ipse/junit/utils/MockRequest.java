package org.krisbox.ipse.junit.utils;

import org.krisbox.ipse.control.ControlStrings;
import org.springframework.mock.web.MockHttpServletRequest;

import javax.servlet.http.Cookie;

public class MockRequest extends ControlStrings {
    String cookieName;
    String cookieValue;

    public MockRequest(String cookieName, String cookieValue) {
        super();
        this.cookieName  = new String(cookieName);
        this.cookieValue = new String(cookieValue);
    }

    public MockHttpServletRequest createMockRequest() {
        MockHttpServletRequest servletRequest = new MockHttpServletRequest();
        Cookie[] cookies = {new Cookie(cookieName, cookieValue)};
        servletRequest.setCookies(cookies);
        return servletRequest;
    }
}
