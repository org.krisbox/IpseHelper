package org.krisbox.ipse.junit.ipse;

import org.krisbox.json.ipse.utils.IpseHelper;

import javax.servlet.http.HttpServletRequest;

public class DummyIpse extends IpseHelper {
    public DummyIpse(String cookieName) {
        super(cookieName);
    }

    @Override
    public boolean authenticate(HttpServletRequest request) {
        mapJson(request);

        return true;
    }
}
